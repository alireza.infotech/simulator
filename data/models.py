import sys

try:
    from django.db import models
except Exception as e:
    print("There was an error loading django modules. Do you have django installed?")
    sys.exit()

DeviceTypeNamesMap = {
    (1, 'Smartphone'),
    (2, 'Car'),
    (3, 'Tablet'),
    (4, 'Smart Fitness'),
    (5, 'Smartwatch'),
    (6, 'Pc'),
    (7, 'Printer'),
    (8, 'Home Sensors'),
    (9, 'Point of Interest'),
    (10, 'Environment and Weather'),
    (11, 'Transportation'),
    (12, 'Indicator'),
    (13, 'Garbage Truck'),
    (14, 'Street Light'),
    (15, 'Parking'),
    (16, 'Alarms'),
}


class DeviceType(models.Model):
    name = models.IntegerField(default=0, blank=False, choices=DeviceTypeNamesMap)
    id_off_service_1 = models.IntegerField(default=0, blank=True)
    id_off_service_2 = models.IntegerField(default=0, blank=True)
    id_off_service_3 = models.IntegerField(default=0, blank=True)
    id_off_service_4 = models.IntegerField(default=0, blank=True)
    id_off_service_5 = models.IntegerField(default=0, blank=True)
    id_off_service_6 = models.IntegerField(default=0, blank=True)
    id_off_service_7 = models.IntegerField(default=0, blank=True)
    id_off_service_8 = models.IntegerField(default=0, blank=True)
    id_off_service_9 = models.IntegerField(default=0, blank=True)
    id_req_application_1 = models.IntegerField(default=0, blank=True)
    id_req_application_2 = models.IntegerField(default=0, blank=True)
    id_req_application_3 = models.IntegerField(default=0, blank=True)
    id_req_application_4 = models.IntegerField(default=0, blank=True)
    id_req_application_5 = models.IntegerField(default=0, blank=True)
    id_req_application_6 = models.IntegerField(default=0, blank=True)
    id_req_application_7 = models.IntegerField(default=0, blank=True)
    id_req_application_8 = models.IntegerField(default=0, blank=True)
    id_req_application_9 = models.IntegerField(default=0, blank=True)
    id_req_application_10 = models.IntegerField(default=0, blank=True)
    id_req_application_11 = models.IntegerField(default=0, blank=True)
    id_req_application_12 = models.IntegerField(default=0, blank=True)
    id_req_application_13 = models.IntegerField(default=0, blank=True)
    id_req_application_14 = models.IntegerField(default=0, blank=True)
    id_req_application_15 = models.IntegerField(default=0, blank=True)
    id_req_application_16 = models.IntegerField(default=0, blank=True)
    id_req_application_17 = models.IntegerField(default=0, blank=True)
    id_req_application_18 = models.IntegerField(default=0, blank=True)


class User(models.Model):
    alpha = models.FloatField(default=0)
    beta = models.FloatField(default=0)
    gama = models.FloatField(default=0)

    def __str__(self):
        return str(self.pk)


class Device(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='user')
    type = models.ForeignKey(DeviceType, on_delete=models.DO_NOTHING)
    brand = models.IntegerField(default=0)
    model = models.IntegerField(default=0)



