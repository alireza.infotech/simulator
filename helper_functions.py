import pandas as pd
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from data.models import *

def help():

    print('')
    print(" \"create\" : to create Sponsor/Application/Thing ")
    print('')
    print(" \"topo\" : to view designed system")
    print('')
    print(" \"back\" : go back to the first step")
    print('')
    print(" \"help\" : to show more information about using the system")
    print('')
    print(" \"evaluate\" : to evaluate things in each sponsor")
    print('')
    print(" \"show details\" : to show details of evaluation")
    print('')
    print(" \"pref\" : Performance function of each thing using given preferences")
    print('')
    print(" \"sub\" : show available things in each application range briefly")
    print('')
    print(" \"positions\" : show position of each created Sponsor , Application , Things")
    print('')
    print(" \"random\" : create a random scenario")
    print('')
    print(" \"graphic\" : graphical show of scenario")
    print('')
    print(" \"added value\" : calculating the added value of each things in each app")
    print('')
    print(" \"avg of values\" :details of assigned values and fees ")
    print('')
    print(" \"fee cal\" : calculating fee of each thing ")
    print('')
    print(" \"knapsack\" : find the best subset using knapsack algorithm")
    print('')
    print(" \"brute force\" : find the best subset using brute force algorithm")
    print('')
    print(" \"dummy\" : find the best subset using dummy algorithm")
    print('')
    print(" \"explaination\" : to explain details of subsets in each algorithm")
    print('')
    print(" \"results\" : to show the resluts of simulation")
    print('')
    print(" \"quit\" : to exit from the system")


def load_files():
    try:

        df = pd.read_csv("objects_profile.csv", "objects_profile")
        for index, row in df.iterrows():
            cur = str(row.values[0]).split(',')
            print(cur)

        return
        df = pd.read_csv("objects_description.csv", "objects_description")
        print(1111111)
        for index, row in df.iterrows():
            cur = str(row.values[0]).split(',')
            if len(cur) == 5:
                print(2)
                device = Device()
                device.pk = cur[0]
                user, created = User.objects.get_or_create(pk=cur[1])
                device.user = user
                device.type = cur[2]
                device.brand = cur[3]
                device.model = cur[4]
                device.save()
            break
    except Exception as e:
        print(e)