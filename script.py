import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
from data.models import *
from helper_functions import *


def terminal():

    bash = ""
    print("Plesae type  \"help\" to see how can you use the system ")
    while True:
        try:
            bash = input("command $ ")
        except KeyboardInterrupt:
            if bash == "":
                continue
            # TOPO
            elif bash == "topo":
                try:
                    load_files()
                except:
                    print("Error")
                    continue
            #HELP

            elif bash == "help":
                help()

            elif bash.lower() == "quit":
                break
            else:
                print("Wrong command ! ")
                continue
            break


if __name__ == '__main__':
    terminal()
